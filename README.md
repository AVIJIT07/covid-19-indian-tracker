# COVID-19 INDIAN TRACKER

This is a corona virus tracker for India. It includes state wise no. of corona cases and other details made on tableau.
It is published in tableau server. The data update from google sheets.

The link is: https://public.tableau.com/shared/S78XPCRG3?:toolbar=n&amp;:display_count=y&amp;:origin=viz_share_link

The data is scraped from : https://docs.google.com/spreadsheets/d/e/2PACX-1vSc_2y5N0I67wDU38DjDh35IZSIS30rQf7_NYZhtYYGU1jJYT6_kDx4YpF-qw0LSlGsBYP8pqM_a1Pd/pubhtml#